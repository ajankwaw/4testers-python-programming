#def print_greeting_message(name, city):
#    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście: {city}")

#michal_torun = print_greeting_message("Michał", "Toruń")
#Beata_gdynia = print_greeting_message("Beata", "Gdynia")

#z funkcją capitalize
def print_greeting_message(name, city):
    name_capitalized = name.capitalize()
    city_capitalized = city.capitalize()
    print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}") #capitalize to metoda, ktra wykonywana jest na zmiennej, lub funkcji. poprzez kropkę

print_greeting_message("michał", "toruń")
print_greeting_message("Beata", "Gdynia")

#stare
def print_greeting_message(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście: {city}")

print_greeting_message("Michał", "Toruń")
print_greeting_message("Beata", "Gdynia")


def get_4tester_email(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl" #lower to metoda wykonywana na parametrze last_name


print(get_4tester_email("Janusz", "Nowak"))
print(get_4tester_email("Barbara", "Kowalska"))