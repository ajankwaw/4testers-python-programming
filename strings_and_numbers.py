first_name = "Andrzej"
last_name = "Jankowski"
email = "andrija.jankovic1234@gmail.com"

print("Mam na imię ", first_name, ". ", "Moje nazwisko to ", last_name, ".", sep="")

my_bio = "Mam na imię " + first_name + ". Moje nazwisko to " + last_name + ". Mój email to " + email + "."
print(my_bio)

# F-string
my_bio_using_f_string = f"Mam na imię {first_name}. \nMoje nazwisko to {last_name}. \nMój email to {email}."
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4 przez 5 to {4*5}")

###Algebra###

area_of_a_circle_with_radius_5 = 3.1415 * 5 ** 2
circumference_of_a_circle_with_radius_5 = 2 * 3.1415 * 5

print("Area of a circle with radius 5:", area_of_a_circle_with_radius_5)
print("Circumference of a circle with radius 5:", circumference_of_a_circle_with_radius_5)

circle_radius = 50
area_of_a_circle_with_radius = 3.1415 * circle_radius ** 2
circumference_of_a_circle_with_radius = 2 * 3.1415 * circle_radius

print(f"Area of a circle with radius {circle_radius}:", area_of_a_circle_with_radius)
print(f"Circumference of a circle with radius {circle_radius}:", circumference_of_a_circle_with_radius)

long_mathematical_expression = 2 + 3 + 5 *7 + 4/3 * (3 + 5 / 2) + 7 **2 - 13
print("Result of long mathematical expression is:", long_mathematical_expression)