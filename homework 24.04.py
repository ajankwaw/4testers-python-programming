def get_average(numbers_list):
    return sum(numbers_list)/len(numbers_list)

january = [-4, 1.0, -7, 2]
february = [-13, -9, -3, 3]

print(get_average(january))
print(get_average(february))

if __name__ == '__main__':
