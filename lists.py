movies = ['W pogoni za szczęściem','Matrix', 'Chłopiec w pasiastej piżamie', 'Król Lew', 'Jestem Bogiem']

emails = ['a@example.com','b@example.com']

number_of_emails = len(emails)
print(number_of_emails)
print(emails[0])
print(emails[-1])

emails.append("cde@example.com")
print(emails)

friend = {
    'name': 'Adrian',
    'age': '38',
    'interests' : ['law','football']
}
print(friend['age'])
friend['city'] = 'Wrocław'
friend['age'] = 59
print(friend)

del friend ['age']
print(friend["interests"][-1])


def